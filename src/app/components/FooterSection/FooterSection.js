import React from "react";
import { makeStyles, Paper, Typography } from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
  wrapper: {
    height: "30vh",
    margin: theme.spacing(2),
    padding: theme.spacing(2),
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
  },
}));

const FooterSection = () => {
  const classes = useStyles();

  return (
    <Paper className={classes.wrapper}>
      <Typography paragraph>
        Footer section. Well, I'm old plain footer, but You can be creative with
        me.
      </Typography>
      <Typography paragraph>
        Fyzioterapie Mgr. Josef Novák, Pod Pánví 8, Poděbrady, 702 10
      </Typography>
    </Paper>
  );
};

export default FooterSection;
