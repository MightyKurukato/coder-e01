const mockData = [
  {
    procedure: "Procedure 1",
    description:
      "Mollit aliqua nostrud dolore est id. Esse ad consequat laborum ullamco do fugiat non sunt aliquip irure. Aliqua exercitation voluptate nostrud sit in voluptate pariatur esse magna enim irure.",
    duration: 30,
    rate: 300,
  },
  {
    procedure: "Procedure 2",
    description:
      "Mollit aliqua nostrud dolore est id. Esse ad consequat laborum ullamco do fugiat non sunt aliquip irure. Aliqua exercitation voluptate nostrud sit in voluptate pariatur esse magna enim irure.",
    duration: 60,
    rate: 600,
  },
  {
    procedure: "Procedure 3",
    description:
      "Mollit aliqua nostrud dolore est id. Esse ad consequat laborum ullamco do fugiat non sunt aliquip irure. Aliqua exercitation voluptate nostrud sit in voluptate pariatur esse magna enim irure.",
    duration: 20,
    rate: 100,
  },
  {
    procedure: "Procedure 4",
    description:
      "Mollit aliqua nostrud dolore est id. Esse ad consequat laborum ullamco do fugiat non sunt aliquip irure. Aliqua exercitation voluptate nostrud sit in voluptate pariatur esse magna enim irure.",
    duration: 15,
    rate: 300,
  },
  {
    procedure: "Procedure 5",
    description:
      "Mollit aliqua nostrud dolore est id. Esse ad consequat laborum ullamco do fugiat non sunt aliquip irure. Aliqua exercitation voluptate nostrud sit in voluptate pariatur esse magna enim irure.",
    duration: 30,
    rate: 250,
  },
  {
    procedure: "Procedure 6",
    description:
      "Mollit aliqua nostrud dolore est id. Esse ad consequat laborum ullamco do fugiat non sunt aliquip irure. Aliqua exercitation voluptate nostrud sit in voluptate pariatur esse magna enim irure.",
    duration: 60,
    rate: 800,
  },
];

export default mockData;
