import { Fab, Fade, makeStyles } from "@material-ui/core";
import { KeyboardArrowUp } from "@material-ui/icons";
import React, { useState } from "react";
import useEventListener from "./../hooks/useEventListener";

const useStyles = makeStyles((theme) => ({
  fab: {
    position: "fixed",
    bottom: theme.spacing(3),
    right: theme.spacing(3),
  },
}));

const ScrollToTop = () => {
  const classes = useStyles();
  const [showFab, setShowFab] = useState(false);

  useEventListener("scroll", () => {
    if (!showFab && window.pageYOffset > 400) {
      setShowFab(true);
    } else if (showFab && window.pageYOffset <= 400) {
      setShowFab(false);
    }
  });

  const handleScrollToTop = () => {
    window.scrollTo({ top: 0, behavior: "smooth" });
  };

  return (
    <Fade in={showFab} timeout={500}>
      <Fab
        className={classes.fab}
        aria-label={"Scroll to top"}
        color={"primary"}
        onClick={handleScrollToTop}
      >
        <KeyboardArrowUp />
      </Fab>
    </Fade>
  );
};

export default ScrollToTop;
