import { createSlice } from '@reduxjs/toolkit';

const initialState = {
  value: 0,
};

export const rootSlice = createSlice({
  name: 'root',
  initialState,
  reducers: {
    increment: (state) => {
      state.value += 1;
    },
    decrement: (state) => {
      state.value -= 1;
    },
    incrementByAmount: (state, action) => {
      state.value += action.payload;
    },
  },
});

export const { increment, decrement, incrementByAmount } = rootSlice.actions;

export const selectCount = (state) => state.root.value;

export default rootSlice.reducer;
