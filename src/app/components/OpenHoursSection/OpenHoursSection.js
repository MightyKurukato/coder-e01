import {
  Grow,
  makeStyles,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  Typography,
} from "@material-ui/core";
import React from "react";
import { useInView } from "react-intersection-observer";

const mockTableData = [
  { day: "Po", from: "8:00", to: "17:00" },
  { day: "Ut", from: "7:00", to: "17:00" },
  { day: "St", from: "8:00", to: "20:00" },
  { day: "Čt", from: "7:00", to: "20:00" },
  { day: "Pá", from: "8:00", to: "17:00" },
  { day: "So", from: "8:00", to: "12:00" },
];

const useStyles = makeStyles({
  table: {
    minWidth: 350,
    maxWidth: 600,
    margin: "3rem",
  },
});

const OpenHoursSection = () => {
  const classes = useStyles();
  const { ref, inView } = useInView({ threshold: 0.4 });

  return (
    <>
      <Typography paragraph>
        I do not need to be a table. Feel free to present me differently.
      </Typography>
      <Typography variant={"h5"}>Opening Hours</Typography>
      <Grow in={inView} timeout={1000} ref={ref}>
        <Table className={classes.table} aria-label="simple table">
          <TableHead>
            <TableRow>
              <TableCell>Day</TableCell>
              <TableCell align="right">From</TableCell>
              <TableCell align="right">To</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {mockTableData.map(({ day, from, to }) => (
              <TableRow key={day}>
                <TableCell component="th" scope="row">
                  {day}
                </TableCell>
                <TableCell align="right">{from}</TableCell>
                <TableCell align="right">{to}</TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </Grow>
    </>
  );
};

export default OpenHoursSection;
