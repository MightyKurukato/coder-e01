import { CssBaseline, MuiThemeProvider, NoSsr } from "@material-ui/core";
import React from "react";
import { ThemeProvider } from "styled-components";
import FooterSection from "./app/components/FooterSection/FooterSection";
import HeroSection from "./app/components/HeroSection/HeroSection";
import MainSection from "./app/components/MainSection/MainSection";
import OpenHoursSection from "./app/components/OpenHoursSection/OpenHoursSection";
import RatesSection from "./app/components/RatesSection/RatesSection";
import ScrollToTop from "./app/components/ScrollToTop";
import customTheme from "./app/custom-theme";

const App = () => {
  return (
    <NoSsr>
      <MuiThemeProvider theme={customTheme}>
        <ThemeProvider theme={customTheme}>
          <CssBaseline />
          <HeroSection />
          <MainSection />
          <OpenHoursSection />
          <RatesSection />
          <FooterSection />
          <ScrollToTop />
        </ThemeProvider>
      </MuiThemeProvider>
    </NoSsr>
  );
};

export default App;
