import {
  Accordion,
  AccordionDetails,
  AccordionSummary,
  Container,
  makeStyles,
  Slide,
  Typography,
} from "@material-ui/core";
import { ExpandMoreOutlined } from "@material-ui/icons";
import React from "react";
import { useInView } from "react-intersection-observer";
import mockData from "./../../mock-data";

const useStyles = makeStyles((theme) => ({
  root: {
    width: "50%",
  },
  title: {
    width: "100%",
    textAlign: "center",
  },
  heading: {
    flexBasis: "80%",
    flexShrink: 0,
  },
  secondaryHeading: {
    fontSize: theme.typography.pxToRem(20),
    color: theme.palette.text.secondary,
  },
}));

const RatesSection = () => {
  const classes = useStyles();
  const { ref, inView } = useInView({ threshold: 0.3 });

  return (
    <Container ref={ref}>
      <Slide
        in={inView}
        direction={"left"}
        timeout={1000}
        className={classes.title}
      >
        <Typography variant={"h5"} paragraph>
          I do display rates of procedures, feel free to change the
          visualisation
        </Typography>
      </Slide>
      <Slide in={inView} direction={"right"} timeout={1000}>
        <Container className={classes.root}>
          {mockData.map(({ procedure, description, rate }) => (
            <Accordion key={procedure}>
              <AccordionSummary expandIcon={<ExpandMoreOutlined />}>
                <Typography className={classes.heading}>{procedure}</Typography>
                <Typography className={classes.secondaryHeading}>
                  {`${rate} Kč`}
                </Typography>
              </AccordionSummary>
              <AccordionDetails>
                <Typography>{description}</Typography>
              </AccordionDetails>
            </Accordion>
          ))}
        </Container>
      </Slide>
    </Container>
  );
};

export default RatesSection;
