import { Box } from "@material-ui/core";
import styled from "styled-components";

const FullPageCenteredWrapper = styled(Box)`
  display: flex;
  flexdirection: column;
  justifycontent: center;
  alignitems: center;
  min-height: 100vh;
`;

export default FullPageCenteredWrapper;
