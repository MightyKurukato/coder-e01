import { Box, Button, Grow, Typography } from "@material-ui/core";
import React from "react";

const HeroSection = () => {
  return (
    <Grow in>
      <Box
        display={"flex"}
        flexDirection={"column"}
        justifyContent={"center"}
        alignItems={"center"}
        height={"100vh"}
      >
        <Typography color={"primary"} variant={"h4"} paragraph>
          I'm hero section wow me.
        </Typography>
        <Button color={"secondary"}>Click me I do nothing</Button>
      </Box>
    </Grow>
  );
};

export default HeroSection;
